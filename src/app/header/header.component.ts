import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';

import { DataStorageService } from '../shared/data-storage.service';
import { Subscription } from 'rxjs';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnDestroy {
  subscription: Subscription;

  constructor(private dataStoregService: DataStorageService,
              private authService: AuthService) {
  }

  onSaveData() {
    this.subscription = this.dataStoregService.storeRecipes()
      .subscribe(
        response => console.log(response)
      );
  }

  onFetchData() {
    this.dataStoregService.getRecipes();
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  onLogout() {
    this.authService.logout();
  }
}
